<section class="Construct-Wrap">

    <h2>Собери свой мешок Sandy</h2>

    <div class="Construct-Border">
        <div class="Construct">
            <div class="Construct-Bag">

                <div class="Bag-Wrap">
                    <div class="Bag-Weight">
                        <span></span>
                        <?php require $Dir_Images . 'wages.svg'; ?>

                    </div>
                    <div class="Bag">
                        <div class="Body-Wrap">
                            <div class="Bag-Body"></div>
                            <div class="Bag-Width">
                                <span>60см</span>
                            </div>
                        </div>

                    </div>
                    <div class="Bag-Height">
                        <span>120см</span>
                    </div>
                </div>
            </div>
            <div class="Construct-Description">
                <div class="Construct-Info">

                    <span class="Info-Title">Высота</span>

                    <div class="Info JS-Script-Menu">
                        <div class="JS-Click-Left Previous">
                            <?php require $Dir_Images . 'arrow.svg'; ?>
                        </div>
                        <div class="Construct-Button JS-Menu-Button">
                            <span class="Height-Result"></span>
                        </div>
                        <ul class="Info-Height JS-Menu-List"></ul>
                        <div class="JS-Click-Right Next">
                            <?php require $Dir_Images . 'arrow.svg'; ?>
                        </div>
                    </div>
                </div>
                <div class="Construct-Info">

                    <span class="Info-Title">Диаметр</span>

                    <div class="Info JS-Script-Menu">

                        <div class="JS-Click-Left Previous">
                            <?php require $Dir_Images . 'arrow.svg'; ?>
                        </div>
                        <div class="Construct-Button JS-Menu-Button">
                            <span class="Diameter-Result"></span>
                        </div>
                        <ul class="Info-Diameter JS-Menu-List"></ul>
                        <div class="JS-Click-Right Next">
                            <?php require $Dir_Images . 'arrow.svg'; ?>
                        </div>

                    </div>
                </div>
                <div class="Construct-Info">
                    <span class="Info-Title">Вес</span>

                    <div class="Info JS-Script-Menu">
                        <div class="JS-Click-Left Previous">
                            <?php require $Dir_Images . 'arrow.svg'; ?>
                        </div>
                        <div class="Construct-Button JS-Menu-Button">
                            <span class="Weight-Result"></span>
                        </div>
                        <ul class="Info-Weight JS-Menu-List"></ul>
                        <div class="JS-Click-Right Next">
                            <?php require $Dir_Images . 'arrow.svg'; ?>
                        </div>
                    </div>
                </div>

                <div class="Info-Summery">
                    <p>Стоимость:
                        <span class="Price-Result"></span>
                        <svg>
                            <use xlink:href="#rouble"></use>
                        </svg>
                    </p>
                    <button class="Info-Order">Заказать эту</button>
                </div>
            </div>
        </div>

</section>
<!---->
<!---->
<!---->
<!---->
<!---->
<!---->
<!---->
<!---->
<!---->
<!---->
<!---->
<!---->
<!---->
<!--<section class="Construct-Wrap">-->
<!---->
<!--    <h2>Собери свой мешок Sandy</h2>-->
<!---->
<!--    <div class="Construct-Border">-->
<!--        <div class="Construct Second">-->
<!--            <div class="Construct-Bag Second">-->
<!---->
<!--                <div class="Bag-Wrap">-->
<!--                    <div class="JS-Click-Left Previous Second">-->
<!--                        --><?php //require $Dir_Images . 'arrow.svg'; ?>
<!--                    </div>-->
<!--                    <div class="Bag-Center">-->
<!--                        <div class="Bag-Weight">-->
<!--                            <span></span>-->
<!--                            --><?php //require $Dir_Images . 'wages.svg'; ?>
<!---->
<!--                        </div>-->
<!--                        <div class="Bag">-->
<!---->
<!--                            <div class="Body-Wrap">-->
<!--                                <div class="Bag-Body"></div>-->
<!--                                <div class="Bag-Width">-->
<!--                                    <span></span>-->
<!--                                </div>-->
<!--                            </div>-->
<!---->
<!--                        </div>-->
<!--                        <div class="Bag-Height">-->
<!--                            <span></span>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                    <div class="JS-Click-Right Next Second">-->
<!--                        --><?php //require $Dir_Images . 'arrow.svg'; ?>
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
<!--            <div class="Construct-Description Second">-->
<!---->
<!--                <div class="Info-Summery Second">-->
<!--                    <p>Стоимость:-->
<!--                        <span class="Price-Result"></span>-->
<!--                        <svg>-->
<!--                            <use xlink:href="#rouble"></use>-->
<!--                        </svg>-->
<!--                    </p>-->
<!--                    <button class="Info-Order">Заказать эту</button>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
<!---->
<!--</section>-->

<script>
    var Prices = [
        <?php foreach($Prices as $Price){ ?>
        {
            'height': <?=$Price['height']?>,
            'diameter':<?=$Price['diameter']?>,
            'weight': <?=$Price['weight']?>,
            'price': <?=$Price['price']?>
        },
        <?php }?>
    ];
</script>

