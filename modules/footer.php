<div class="Footer-Wrap">
    <footer>
        <div class="Copyright">
            <span>© 2016 Боксерские мешки Sandy</span>

        </div>
        <div class="Contact-Info JS-Modal-Button" data-modal-id="Modal-Contacts">
            <span>Контактная информация</span>
        </div>
        <div class="Footer-Links">
            <div class="Footer-Socials">
                <a class="Social-Link" href="https://www.youtube.com/">
                    <svg>
                        <use xlink:href="#youtube"></use>
                    </svg>

                </a>
                <a class="Social-Link" href="http://vk.com/sandy_box">
                    <svg>
                        <use xlink:href="#vk"></use>
                    </svg>

                </a>

                <div class="Made-By">
                    <span>Сделано в  <a href="http://progress-time.ru">Progress-Time</a></span>
                </div>
            </div>
        </div>

    </footer>
</div>