<div class="Svg-Container">
    <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
         viewBox="992.5 870.5 1589 480">
        <defs>
            <linearGradient id="a" gradientUnits="userSpaceOnUse" x1="992.5352" y1="1110.4954" x2="1472.5352"
                            y2="1110.4954">
                <stop offset="0" style="stop-color:#FF661F"/>
                <stop offset="1" style="stop-color:#DB340E"/>
            </linearGradient>
        </defs>
    </svg>
</div>