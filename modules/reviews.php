<div class="Reviews-Wrap">
    <section class="Reviews JS-Carousel">
        <h2>Отзывы клиентов</h2>


        <ul class="JS-Carousel-List">
            <?php require_once __DIR__ ."/one-review.php"; ?>
        </ul>
        <div class="JS-Click-Left Previous">
            <?php require $Dir_Images . 'arrow.svg'; ?>
        </div>

        <div class="Previous JS-Carousel-Previous">
            <?php require $Dir_Images . 'arrow.svg'; ?>
        </div>
        <div class="Next JS-Carousel-Next">
            <?php require $Dir_Images . 'arrow.svg'; ?>
        </div>
    </section>
</div>