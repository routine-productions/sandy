<div class="JS-Modal" id="Modal-Contacts">
    <div class="Contacts-Wrap JS-Modal-Box">
        <h2>Контактная информация</h2>

        <svg class="JS-Modal-Close">
            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#close"></use>
        </svg>

        <div class="Contacts-Map">
            <script type="text/javascript" charset="utf-8"
                    src="https://api-maps.yandex.ru/services/constructor/1.0/js/?sid=L4S__MLm7V0sutl_mQg4DG2MqXh9olFQ&width=100%&height=400&lang=ru_UA&sourceType=constructor"></script>
        </div>
        <div class="Contacts-Info">
            <h3>Отдел продаж в г. Владимир:</h3>
            <ul>
                <li>ул. Электрозаводская 1,</li>
                <li>+7 (4922) 60-02-53</li>
                <li>+7 (4922) 60-01-86</li>
            </ul>
        </div>


    </div>
</div>