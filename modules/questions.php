<section class="Questions-Wrap">
    <h2>Вопросы и ответы</h2>
    <ul class="Questions">
        <li>
            <h4>Как сделать заказ?</h4>

            <p>Заполнить форму Имя, телефон и нажать кнопку купить. Вам позвонит менеджер и уточнит город доставки,
                модель мешка и условия оплаты. </p>
        </li>
        <li>
            <h4>Доставка включена в стоимость заказа?</h4>

            <p>Нет, доставка оплачивается заказчиком.</p>
        </li>
        <li>
            <h4>Ваш товар есть в Наличии? Сколько нужно ждать?
            </h4>

            <p>Самые ходовые мешки есть в наличии на складе! Также мы шьем индивидуальные мешки. Срок пошива 1
                день. </p>
        </li>
        <li>
            <h4>С какими перевозчиками вы работаете?</h4>

            <p>
                Мы можем отправить вам мешок следующими транспортными компаниями:
            </p>
            <ul>
                <li>Деловые линии</li>
                <li>DPD</li>
                <li>Major</li>
            </ul>
        </li>
        <li>
            <h4>Оплата производится по 100% предоплате или при получении?</h4>

            <p>Отправка товара производиться только после 100% предоплаты заказа!</p>
        </li>
        <li>
            <h4>Работаете с бюджетными организациями?</h4>

            <p>Да, работаем! По договору.</p>
        </li>
    </ul>
    <div class="Consult-Wrap">
        <section class="Consult">
            <form class="JS-Form"
                  data-form-email="offer@sandy24.ru,olegblud@gmail.com"
                  data-form-subject="Order form website"
                  data-form-url="/js/data.form.php"
                  data-form-method="POST" data-form-error="Заполните все поля!">
                <h4>Нужна консультация?</h4>
                <div class="Consult-Input">
                    <span>
                        <label for="Name">Ваше имя</label>
                        <input class="JS-Form-Require" name="name" data-input-title="Имя" type="text" id="Name"
                               placeholder="Введите Ваше имя"/>
                    </span>
                </div>
                <div class="Consult-Input">
                    <span><label for="Phone">Телефон</label>
                        <input class="JS-Form-Require JS-Form-Mask" data-input-title="Телефон" name="phone" type="text"
                               id="Phone"
                               placeholder="+8 (XXX) XXX XX XX"/>
                    </span>
                </div>
                <button class="JS-Form-Button"> <span class="JS-Form-Result Inside-Consult"></span>Перезвоните мне</button>

            </form>
        </section>
    </div>
</section>