<div class="Container-Wrap">
    <div class="Container-Block Position_Left">
        <div class="Offer">
            <h1>
            <span class="Offer-Row">
                <span class="Offer-Back">
                    <span>качественные</span>
                </span>
            </span>
            <span class="Offer-Row">
                <span class="Offer-Back">
                    <span>боксёрские мешки</span>
                </span>
            </span>
            <span class="Offer-Row">
                <span class="Offer-Back">
                    <span>от 2000 рублей</span>
                </span>
            </span>
            </h1>
        </div>
    </div>

    <div class="Container-Block Position_Right">
        <div class="Order">
            <h3>Заказать консультацию</h3>

            <form class='JS-Form'
                  data-form-email='offer@sandy24.ru,olegblud@gmail.com'
                  data-form-subject='Order form website'
                  data-form-url='/js/data.form.php'
                  data-form-method='POST'
                  data-form-error="Заполните все поля!">
                <label>Ваше имя</label>
                <input class="JS-Form-Require" type="text" name='name' data-input-title='Имя'
                       placeholder="Введите ваше имя">
                <label>Ваш телефон</label>
                <input class="JS-Form-Require JS-Form-Mask" type="text" name='phone' placeholder="7 (916) 999-99-99"
                       data-input-title='Телефон'>

                <button class="Order-Recall JS-Form-Button">
                    Перезвоните мне
                </button>
                <div class="JS-Form-Result"></div>
            </form>
        </div>
    </div>
</div>

