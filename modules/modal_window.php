<div class="JS-Modal" id="Modal-Buy">
    <div class="Modal-Window JS-Modal-Box">
        <h3>Выберите подходящий вам вариант мешка</h3>

        <form class='JS-Form'
              data-form-email='offer@sandy24.ru,olegblud@gmail.com'
              data-form-subject='Order form website'
              data-form-url='/js/data.form.php'
              data-form-method='POST'
              data-form-error="Заполните все поля и выберите мешок!">

            <div class="List">
                <div class="List-Title">
                    <span class="List-Size">Размеры</span>
                    <span class="List-Weight">Вес</span>
                    <span class="List-Count"></span>
                    <span class="List-Price">Стоимость</span>
                </div>
                <ul>
                    <?php foreach ($Prices as $Price) { ?>
                        <li>
                            <span class="List-Back"></span>
                        <span class="List-Size JS-Span-Input" data-name="boxing-size"
                              data-input-title='Your Boxing Size'><?= $Price['height'] ?>*<?= $Price['diameter'] ?>
                            см</span>
                        <span class="List-Weight JS-Span-Input" data-name="boxing-weight"
                              data-input-title='Your Boxing Weight'><?= $Price['weight'] ?> кг</span>
                        <span class="List-Count">
                            <span class="Less"><</span>
                            <span class="Count"><span>1</span> шт.</span>
                            <span class="More">></span>
                        </span>
                        <span class="List-Price Orange JS-Span-Input" data-name="boxing-price"
                              data-input-title='Your Boxing Price'>
                            <span class="Integer"><?= $Price['price'] ?></span><sup>00</sup>
                            <span class="Rouble">
                                 <svg>
                                     <use xlink:href="/images/sprite.svg#ruble"></use>
                                 </svg>
                            </span>
                        </span>
                        </li>
                    <?php } ?>
                </ul>
            </div>

            <div class="Payment-Contacts">
                <div class="Payment-Block">
                    <label>Ваше имя</label>
                    <input class="JS-Form-Require" type="text" name='name'
                           data-input-title='Имя' placeholder="Введите ваше имя">
                </div>
                <div class="Payment-Block">
                    <label>Ваш телефон</label>
                    <input class="JS-Form-Require JS-Form-Mask" type="text" name='phone'
                           data-input-title='Телефон' placeholder="8 (012) 123-09-43">

                    <input type="hidden" name="price"  data-input-title='Общая стоимость'/>
                    <input type="hidden" name="message"  data-input-title='Сообщение'/>
                </div>


            </div>
            <div class="Summary"></div>
            <button class="Pay JS-Form-Button">Перейти к оплате</button>
            <div class="JS-Form-Result"></div>
        </form>


        <div class="Close-Modal JS-Modal-Close">
            <svg>
                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/images/sprite.svg#close"></use>
            </svg>
        </div>

    </div>
</div>