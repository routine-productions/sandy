<div class="JS-Modal" id="Modal-Success">
    <div class="Modal-Window JS-Modal-Box JS-Vertical-Align">
        <h3>Оплата успешно завершена. Спасибо за покупку!</h3>

        <p>В ближайшее время с Вами свяжется менеджер.</p>

        <div class="Close-Modal JS-Modal-Close">
            <svg>
                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/images/sprite.svg#close"></use>
            </svg>
        </div>
    </div>
</div>
