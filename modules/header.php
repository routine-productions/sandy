<div class="Header">

    <div class="Header-Block">
        <div class="Header-Logo Left">
            <a href="/">
                <svg>
                    <use xlink:href="#logo"></use>
                </svg>
            </a>
        </div>
        <button class="Header-Buy Right JS-Modal-Button" data-modal-id="Modal-Buy">
            Купить!
        </button>
        <img class="Header-Arrow" src="/images/bg-arrow.png" alt="">
    </div>


    <div class="Header-Block Links">
        <div class="Header-Link Left">
            <a href="https://www.youtube.com/" target="_blank">
                <svg class="Header-Youtube">
                    <use xlink:href="#youtube"></use>
                </svg>
                <span>канал на YouTube</span>
            </a>
        </div>
        <div class="Header-Link Right">
            <a href="/images/sandy24-price.pdf" target="_blank">
                <svg class="Header-Pdf">
                    <use xlink:href="#pdf"></use>
                </svg>
                <span>скачать прайс-лист</span>
            </a>
        </div>
    </div>

</div>


