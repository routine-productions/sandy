<section class="Inside-Wrap">
    <h2>Что внутри?</h2>

    <div class="Inside-Background">
        <div class="Inside JS-Tabs">
            <div class="Inside-Width">
                <div class="Inside-Block JS-Tabs-Navigation">
                    <div class="Punchbag">

                        <div class="Back">
                            <div></div>
                        </div>
                        <div class="Filler JS-Tab" data-href="Tab-1">
                            <div>
                                <span class="Inside-Name">Наполнитель</span>
                            </div>
                        </div>
                        <div class="Tarpaulin JS-Tab" data-href="Tab-2">
                            <div>
                                <span class="Inside-Name">Брезент</span>
                            </div>
                        </div>
                        <div class="Case-Cell JS-Tab" data-href="Tab-3">
                            <div>
                                <span class="Inside-Name">Кофр</span>
                            </div>
                        </div>
                        <div class="Top JS-Tab Active" data-href="Tab-4"></div>
                    </div>
                </div>
                <div class="Inside-Block JS-Tabs-Content">
                    <div class="Inside-Tabs" data-tab='Tab-1'>
                    <span>
                        Наполнитель
                    </span>

                        <p>
                            Особое внимание следует уделить вопросу, какой должен быть боксерский мешок, чтобы тренировки
                            были максимально эффективными? Для спортзала мы рекомендуем купить мешок, который позволит
                            прорабатывать серии ударов руками и ногами.
                        </p>
                    </div>
                    <div class="Inside-Tabs" data-tab='Tab-2'>
                    <span>
                        Брезент
                    </span>

                        <p>
                            Боксерский мешок - это универсальный спортивный тренажер для занятий боксом, как в домашних
                            условиях, так и в спортивном зале, либо во дворе. Упражняясь на боксерской груше для дома,
                            атлетам хорошо удается проработать мышцы рук и ног, наладить систему
                        </p>
                    </div>
                    <div class="Inside-Tabs" data-tab='Tab-3'>
                    <span>
                        Кофр
                    </span>

                        <p>
                            ДЛЯ УЛИЦЫ
                            Зимний период загоняет спортсменов в зал. Но вот когда на улице лето – не очень- то хочется в
                            помещение, а физическую форму всегда нужно поддерживать. Работа с боксерским мешком на природе
                            отличный вариант, для поддержания физической
                        </p>
                    </div>
                    <div class="Inside-Tabs" data-tab='Tab-4'>
                    <span>
                        Мешок
                    </span>

                        <p>
                            Это мешок. Не путать ни с чем другим.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="Consult-Wrap">
        <section class="Consult">
            <form class="JS-Form"
                  data-form-email="offer@sandy24.ru,olegblud@gmail.com"
                  data-form-subject="Order form website"
                  data-form-url="/js/data.form.php"
                  data-form-method="POST" data-form-error="Заполните все поля!">
                <h4>Нужна консультация?</h4>

                <div class="Consult-Input">
                    <span>
                        <label for="Name-1">Ваше имя</label>
                        <input class="JS-Form-Require" name="name" data-input-title="Имя" type="text" id="Name-1"
                               placeholder="Введите Ваше имя"/>
                    </span>
                </div>
                <div class="Consult-Input">
                    <span><label for="Phone-1">Телефон</label>
                        <input class="JS-Form-Require JS-Form-Mask" data-input-title="Телефон" name="Phone-1" type="text"
                               id="Phone-1"
                               placeholder="+8 (XXX) XXX XX XX"/>
                    </span>
                </div>
                <button class="JS-Form-Button">Перезвоните мне
                    <span class="JS-Form-Result Inside-Consult"></span>
                </button>

            </form>
        </section>
    </div>

</section>
