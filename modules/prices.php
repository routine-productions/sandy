<?php
$Prices = [
    [
        'height' => 70,
        'diameter' => 30,
        'weight' => 20,
        'price' => 2000
    ],
    [
        'height' => 100,
        'diameter' => 30,
        'weight' => 30,
        'price' => 2500
    ],
    [
        'height' => 120,
        'diameter' => 35,
        'weight' => 40,
        'price' => 3000
    ],
    [
        'height' => 120,
        'diameter' => 40,
        'weight' => 50,
        'price' => 3500
    ],
    [
        'height' => 160,
        'diameter' => 40,
        'weight' => 60,
        'price' => 5000
    ],
    [
        'height' => 160,
        'diameter' => 40,
        'weight' => 80,
        'price' => 5800
    ],

];
?>
