<div class="Advantage-Wrap">
    <section class="Advantage">
        <h2>Почему <span>Sandy</span>?</h2>
        <ul class="Advantage-List">
            <li class="Advantage-Price">
                <?php require $Dir_Images . 'money.svg'; ?>
                <p>Цена от производителя</p>
            </li>
            <li class="Advantage-Moneyback">
                <?php require $Dir_Images . 'moneyback.svg'; ?>
                <p>100% возврат денег</p>
            </li>
            <li class="Advantage-Experience">
                <?php require $Dir_Images . 'cake.svg'; ?>
                <p>11 лет шьем боксерские мешки</p>
            </li>
            <li class="Advantage-Expert">
                <?php require $Dir_Images . 'expert.svg'; ?>
                <p>Мы знаем своё дело</p>
            </li>
            <li class="Advantage-Help">
                <?php require $Dir_Images . 'help.svg'; ?>

                <p>Компетентная помощь в выборе</p>
            </li>
            <li class="Advantage-Individual">
                <?php require $Dir_Images . 'bricks.svg'; ?>
                <p>Выполняем индивидуальные заказы</p>
            </li>
        </ul>
    </section>

</div>
