<!DOCTYPE html>
<html>

<head lang="ru">
    <meta charset="UTF-8">
    <title>Sandy</title>

    <link href='https://fonts.googleapis.com/css?family=Exo+2:400,500,600,700italic,700&subset=latin,cyrillic'
          rel='stylesheet' type='text/css'>

    <link rel="stylesheet" href="/css/index.min.css" type="text/css"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <?php
    require_once './modules/favicon.php';
    require_once './modules/yandex.php';
    ?>
</head>

<body>
<?php
require_once './modules/svg-gradient.php';
require_once __DIR__ . '/libs/Mobile_Detect.php';
$Detect = new Mobile_Detect;
$Dir_Images = __DIR__ . '/images/';
?>


<div class="Wrapper">
    <div class="Head">
        <div class="Back-Video">
            <?php if (!$Detect->isMobile() && !$Detect->isTablet()) { ?>
                <video autoplay loop="loop" preload id="video">
                    <source src="/images/video.mp4" type="video/mp4">
                    <source src="/images/video.webm" type="video/webm">
                    <img src="/images/bg-screenshot.jpg" alt="">
                </video>
            <?php } ?>
        </div>
        <div class="Back-Lines"></div>
        <div class="Header-Container">
            <header>
                <?php require_once './modules/header.php'; ?>
            </header>
            <div class="Header-Content">
                <?php require_once __DIR__ . "/modules/prices.php"; ?>
                <?php require_once __DIR__ . "/modules/container.php"; ?>
            </div>
        </div>
    </div>
    <main>
        <?php require_once __DIR__ . "/modules/advantages.php"; ?>
        <?php require_once __DIR__ . "/modules/inside.php"; ?>
        <?php require_once __DIR__ . "/modules/bag-types.php"; ?>
        <?php require_once __DIR__ . "/modules/reviews.php"; ?>
        <?php require_once __DIR__ . "/modules/construct.php"; ?>
        <?php require_once __DIR__ . "/modules/questions.php"; ?>
        <?php require_once __DIR__ . "/modules/footer.php"; ?>
        <?php require __DIR__ . "/images/sprite.svg"; ?>

    </main>
</div>


<?php require_once __DIR__ . "/modules/modal_window.php"; ?>
<?php require_once __DIR__ . "/modules/modal-contacts.php"; ?>
<?php require_once __DIR__ . "/modules/modal_yandex.php"; ?>
<?php require_once __DIR__ . "/modules/modal_success.php"; ?>

<script src="//code.jquery.com/jquery-1.12.0.min.js"></script>
<script src="index.min.js"></script>
</html>

