/*
 * Copyright (c) 2015
 * Routine JS - Ease Carousel
 * Version 0.1.0 Beta
 * Created 2016.02.09
 * Author Bunker Labs
 */
$.fn.JS_Easy_Carousel = function (Object) {
    var Methods = {};
    Object = $.extend({
        Carousel: this,
        List: this.find('.JS-Carousel-List'),
        Item: '.JS-Carousel-Item',
        Next: $('.JS-Carousel-Next'),
        Previous: $('.JS-Carousel-Previous'),
        Navigation: $('<ul/>', {class: 'JS-Carousel-Navigation Reviews-Nav'}),
        Navigation_Item: $('<li>', {class: 'JS-Carousel-Navigation-Item'}),
        Effect: 'slide', // no | fade | slide
        Height: 'auto', // no | parent | screen
        Index: 0,
        Arrows_Active: true,
        Navigation_Active: true,
        Swipe_Active: true
    }, Object);

    Object.Count = this.find(Object.Item).length;
    Object.Animated = false;

    // Create Carousel
    Methods.Create = function () {
        // Clone Array
        var Items = Object.Carousel.find(Object.Item);
        Object.Items = $.makeArray(Items);
        Items.remove();


        // Append Index Item
        Object.List.append($(Object.Items[Object.Index]).clone());

        // Create Navigation
        if (Object.Navigation_Active) {
            for (var Index = 0; Index < Object.Count; Index++) {
                Object.Navigation.append(Object.Navigation_Item.clone());
            }
            Object.Carousel.append(Object.Navigation);
            $($('li', Object.Navigation).get(Object.Index)).addClass('Active')
                .siblings().removeClass('Active');
        }
    };

    // Change current index
    Methods.Change_Index = function (Direction) {
        Object.Actual_Index = Object.Index;
        if (Direction == 'right') {
            if (Object.Index == 0) {
                Object.Index = Object.Count - 1;
            } else {
                Object.Index--;
            }
        } else if (Direction == 'left') {
            if (Object.Index == Object.Count - 1) {
                Object.Index = 0;
            } else {
                Object.Index++;
            }
        } else {
            Object.Index = $(Object.Event.currentTarget).index();
        }

        $($('li', Object.Navigation).get(Object.Index)).addClass('Active')
            .siblings().removeClass('Active');
    };

    // Animate carousel
    Methods.Animate = function (Direction, Element) {
        if (Direction == 'right' || (Object.Index < Object.Actual_Index && Direction != 'left')) {
            Element.css({'left': '-100%', 'opacity': 0}).delay(1).promise().done(function () {
                Element.css({'left': '0', 'opacity': 1});
                Element.siblings().css({'left': '100%', 'opacity': 0});
            });
        } else if (Direction == 'left' || Object.Index > Object.Actual_Index) {
            Element.css({'left': '100%', 'opacity': 0}).delay(1).promise().done(function () {
                Element.css({'left': '0', 'opacity': 1});
                Element.siblings().css({'left': '-100%', 'opacity': 0});
            });
        }

        $('.Reviews-Wrap').css('background-position', 10 *( Object.Index + 1)+ '% center');
    };

    // Slide
    Methods.Slide = function (Event) {
        // Waiting end of animation
        if (!Object.Animated) {
            Object.Animated = true;
        } else {
            return;
        }

        // Append Element
        Object.Event = Event;
        Methods.Change_Index(Event.data.direction);
        var Element = $(Object.Items[Object.Index]).clone();
        Object.List.append(Element);

        // Animate
        Methods.Animate(Event.data.direction, Element);

        setTimeout(function () {
            Element.siblings().remove();
            Object.Animated = false;
        }, parseFloat(Element.css('transition-duration')) * 1000);
    };

    Methods.Auto_Height = function () {
        $(Object.List, Object.Carousel).height($(Object.Item + ':visible', Object.Carousel).outerHeight());
    };

    $(window).resize(function () {
        Methods.Auto_Height();
    });
    $(window).load(function () {
        Methods.Auto_Height();
    });

    // Events
    Methods.Create();
    $(Object.Previous).click({direction: 'left'}, Methods.Slide);
    $(Object.Next).click({direction: 'right'}, Methods.Slide);
    $('li', Object.Navigation).click({direction: 'navigation'}, Methods.Slide);

    $(Object.List, Object.Carousel).swipe({
        swipeLeft: function () {
            $(Object.Previous).trigger('click');
        },
        swipeRight: function () {
            $(Object.Next).trigger('click');
        },
        allowPageScroll: "auto"
    });


    return this;
};

$('.JS-Carousel').JS_Easy_Carousel();