// Prices List

$('#Modal-Buy .List li').click(function () {
    if ($(this).hasClass('Active')) {
        $(this).removeClass('Active');
    } else {
        $(this).addClass('Active');
    }
});
$('.List-Count .Less').click(function () {
    var $Count = $(this).parents('.List-Count').find('.Count span');
    if (parseInt($Count.text()) > 1) {
        $Count.text(parseInt($Count.text()) - 1);
    }
    return false;
});
$('.List-Count .More').click(function () {
    var $Count = $(this).parents('.List-Count').find('.Count span');
    if (parseInt($Count.text()) < 1000) {
        $Count.text(parseInt($Count.text()) + 1);
    }
    return false;
});

$(' #Modal-Buy .List li,.List-Count .More,.List-Count .Less').click(function () {
    var Text = 'Вы выбрали:<br>';
    var Price = 0;

    $('#Modal-Buy .Active').each(function () {
        var Count = $(this).find('.List-Count .Count span').text(), Count_Text;

        if (Count == 1) {
            Count_Text = Count + ' мешок (';
        } else if (Count > 1 && Count < 5) {
            Count_Text = Count + ' мешка (';
        } else {
            Count_Text = Count + ' мешков (';
        }

        Price += parseInt($(this).find('.List-Price .Integer').text()) * parseInt($(this).find('.List-Count .Count span').text());

        Text += '<strong>' +
            Count_Text +
            $(this).find('.List-Size').text() + ', ' +
            $(this).find('.List-Weight').text() + ', ' +
            $(this).find('.List-Price .Integer').text() + 'р.)' +
            ' на сумму ' + ( $(this).find('.List-Price .Integer').text() * Count) + 'р.' +
            '</strong>';
    });

    if (Price > 0) {
        Text += '<p><strong>Общая сумма: <span class="Total-Price">' + Price + '</span> р.</strong></p>';
    }
    $('[name=price]').val(Price);

    if (Text.length > 15) {
        $('.Summary').html(Text);
        $('[name=message]').val(Text);
    } else {
        $('.Summary').html('');
    }
});

$('#Modal-Buy input[name=count]').keydown(function () {
    $('#Modal-Buy input[name=count]').trigger('click');
});


// Yandex Return

$('.Purchase-Back').click(function () {
    $('#Modal-Yandex').removeClass('Visible');
    $('#Modal-Buy').addClass('Visible');
});

// Show Success Payment
if (location.search == '?payment=success') {
    $('#Modal-Success').addClass('Visible');
}


// Set Video Height
Video_Height();
$(window).resize(function () {
    Video_Height();
});
function Video_Height() {
    if ($(window).width() <= 1200) {
        $('.Back-Video,.Back-Lines').css('height', $('.Order').offset().top);
    } else {
        $('.Back-Video,.Back-Lines').css('height', '');
    }
}

// Mask
$('.JS-Form-Mask').mask('0 (000) 000-00-00', {
    onComplete: function (Result, Event) {
        $(Event.currentTarget).addClass('JS-Form-Masked');
    }, onKeyPress: function (Result, Event) {
        $(Event.currentTarget).removeClass('JS-Form-Masked');
    }
});


