/*
 * Copyright (c) 2015
 * Routine JS - Vertical Align
 * Version 0.1.1
 * Create 2015.12.02
 * Author Bunker Labs
 */
$(document).ready(function ()
{
    Vertical_Align();
    $(window).resize(function ()
    {
        Vertical_Align();
    });

    function Vertical_Align()
    {
        $('.JS-Vertical-Align').each(function ()
        {
            var Element_Shift = $(this).attr('data-shift') ? $(this).attr('data-shift') : false,
                Element_Shift_Selector = $(this).attr('data-shift-selector') ? $(this).attr('data-shift-selector') : false,
                Min_Width = $(this).attr('data-min-width') ? $(this).attr('data-min-width') : 0,
                Max_Width = $(this).attr('data-max-width') ? $(this).attr('data-max-width') : Infinity;

            if (Element_Shift_Selector)
            {
                Element_Shift = $(Element_Shift_Selector).actual('outerHeight');
            }

            if ($(window).width() >= Min_Width && $(window).width() <= Max_Width)
            {
                if ($(this).parent().actual('outerHeight') > $(this).actual('outerHeight'))
                {
                    $(this).css({
                        'margin-top': ($(this).parent().actual('outerHeight') / 2) - ($(this).actual('outerHeight') / 2) + Element_Shift,
                        'position': 'absolute'
                    });
                } else
                {
                    $(this).css({
                        'position': 'absolute'
                    });
                }

            } else
            {
                $(this).css({
                    'margin-top': '',
                    'position': ''
                });
            }
        });
    }
});

