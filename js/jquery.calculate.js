(function ($) {

//Box Calculate

    var Boxing_Index;

    Construct_List();

    function Construct_List() {

        for (var IndexHeight = 0; IndexHeight < Prices.length; IndexHeight++) {
            $(".Info-Height").append('<li>' + Prices[IndexHeight]["height"] + 'см.' + '</li>');
        }

        for (var IndexDiameter = 0; IndexDiameter < Prices.length; IndexDiameter++) {
            $(".Info-Diameter").append('<li>' + Prices[IndexDiameter]["diameter"] + 'см.' + '</li>');
        }

        for (var IndexWeight = 0; IndexWeight < Prices.length; IndexWeight++) {
            $(".Info-Weight").append('<li>' + Prices[IndexWeight]["weight"] + 'кг.' + '</li>');
        }

        Boxing_Index = 0;
        Size_Box(Boxing_Index);
        View(Boxing_Index);
        Weigher(Boxing_Index);
    }


    function Size_Box(Size) {
        $('.Height-Result').text(Prices[Size]["height"] + " см.");
        $('.Diameter-Result').text(Prices[Size]["diameter"] + " см.");
        $('.Weight-Result').text(Prices[Size]["weight"] + " кг.");
        $('.Price-Result').text(Prices[Size]["price"]);

        $('.Bag-Height span').text(Prices[Size]["height"] + " см");
        $('.Bag-Width span').text(Prices[Size]["diameter"] + " см");
        $('.Bag-Weight span').text(Prices[Size]["weight"] + " кг");


        $('.Body-Wrap').css({"width": Prices[Size]["diameter"] * 3, "height": Prices[Size]["height"] * 2.2});
    }


    function View(View_Index) {
        if (View_Index == Prices.length - 1) {
            $(".JS-Click-Right").addClass('Not-Active');
            $(".JS-Click-Left").removeClass('Not-Active');
        } else if (View_Index == 0) {
            $(".JS-Click-Left").addClass('Not-Active');
            $(".JS-Click-Right").removeClass('Not-Active');
        } else {
            $(".JS-Click-Left").removeClass('Not-Active');
            $(".JS-Click-Right").removeClass('Not-Active');
        }

        $($('.Construct-Info .Info-Height.JS-Menu-List li')[View_Index]).addClass('Active').siblings().removeClass('Active');

        $($('.Construct-Info .Info-Diameter.JS-Menu-List li')[View_Index]).addClass('Active').siblings().removeClass('Active');

        $($('.Construct-Info .Info-Weight.JS-Menu-List li')[View_Index]).addClass('Active').siblings().removeClass('Active');
    }

        function Weigher(Weight){
            $('.Wages-Arrow').css({
                "transform":"rotate("+(Prices[Weight]["weight"] * 3)+"deg"+")"
            });
    }

    $('.JS-Click-Left').click(function () {

        if (Boxing_Index > 0) {

            Boxing_Index = Boxing_Index - 1;

            Size_Box(Boxing_Index);
            View(Boxing_Index);
            Weigher(Boxing_Index);

        } else if (Boxing_Index == 0) {
            Boxing_Index = 0;

            Size_Box(Boxing_Index);
            View(Boxing_Index);
            Weigher(Boxing_Index);

        } else {
            return false;
        }

        return false;
    });

    $('.JS-Click-Right').click(function () {

        if (Boxing_Index == Prices.length - 1) {
            Boxing_Index = Prices.length - 1;

            Size_Box(Boxing_Index);
            View(Boxing_Index);
            Weigher(Boxing_Index);


        } else if (Boxing_Index < Prices.length) {
            Boxing_Index = Boxing_Index + 1;

            Size_Box(Boxing_Index);
            View(Boxing_Index);
            Weigher(Boxing_Index);


        } else {
            return false;
        }
        return false;
    });


    //$.fn.DropDownMenu = function () {
    //
    //    var $Global_This = $(this);
    //
    //    $Global_This.find('.JS-Menu-Button').click(function () {
    //
    //        var $Button = $(this);
    //
    //        if ($Button.parents('.JS-Script-Menu').find('.JS-Menu-List').is(':hidden')) {
    //
    //            var Actual_Height = $Button.parents('.JS-Script-Menu').find('.JS-Menu-List *').actual('outerHeight');
    //            console.log(Actual_Height);
    //
    //            $('.JS-Script-Menu .JS-Menu-List').css({'display': 'none', 'height': '0px'});
    //
    //            $Button.parents('.JS-Script-Menu').find('.JS-Menu-List').css({
    //                'display': 'block',
    //                'height': Actual_Height * Prices.length
    //            });
    //
    //        } else {
    //            function Time_Display() {
    //                $Button.parents('.JS-Script-Menu').find('.JS-Menu-List').css({'display': 'none'});
    //            }
    //
    //            $Button.parents('.JS-Script-Menu').find('.JS-Menu-List').css({'height': '0px'});
    //            setTimeout(Time_Display, 200);
    //        }
    //
    //        return false;
    //
    //    });
    //
    //    $(document).find($Global_This).find('.JS-Menu-List').on('click', 'li',
    //
    //        function (event) {
    //
    //            event.stopPropagation();
    //
    //            var Text = $(this).text();
    //
    //            Boxing_Index = $(this).index();
    //
    //            $(this).parents('.JS-Script-Menu').find('.Info-Result').text(Text);
    //
    //            $(this).parents('.JS-Script-Menu').find('.JS-Menu-Button').trigger('click');
    //
    //            Size_Box(Boxing_Index);
    //            View(Boxing_Index);
    //            Weigher(Boxing_Index);
    //
    //            return false;
    //
    //        });
    //
    //
    //    $('body,html').click(function () {
    //
    //        $('.JS-Script-Menu .JS-Menu-List').css({'height': '0'});
    //
    //        function Time_Display_Body() {
    //            $('.JS-Script-Menu .JS-Menu-List').css({'display': 'none'});
    //        }
    //
    //        setTimeout(Time_Display_Body, 200);
    //
    //        return false;
    //
    //    });
    //};


    //$('.Construct-Info').DropDownMenu();


    // Info Order
    $('.Info-Order').click(function () {
        $($('.List li').get(Boxing_Index)).addClass('Active').siblings().removeClass('Active');
        $('.Header-Buy').trigger('click');
    });
})(jQuery);