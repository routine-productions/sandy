//Animate Inside Boxing

var scroll;
var Resize_Width;

Resize_Width = $(window).width();

$(window).resize(function () {

    Resize_Width = $(window).width();

    if (Resize_Width >= 1200) {
        if ($(window).scrollTop() > ($('.Inside-Background').offset().top - 400)) {

            $(".Punchbag .Top, .Punchbag .Back").css("transform", "translateX(-" + 80 * 2 + "px)");
            $(".Punchbag .Filler").css("transform", "translateX(0px)");
            $(".Punchbag .Tarpaulin").css("transform", "translateX(" + 70 * 2 + "px)");
            $(".Punchbag .Case-Cell").css("transform", "translateX(" + 90 * 3 + "px)");

        } else if ($(window).scrollTop() < $('.Inside-Background').offset().top - 400) {

            $(".Punchbag .Top, .Punchbag .Back").css("transform", "translateX(0px)");
            $(".Punchbag .Case-Cell").css("transform", "translateX(0px)");
            $(".Punchbag .Tarpaulin").css("transform", "translateX(0px)");
            $(".Punchbag .Filler").css("transform", "translateX(0px)");

        } else {
            return false;
        }

        Inside_Name();

    } else {
        $('.Inside-Name').addClass("Active");

        $(".Punchbag .Top, .Punchbag .Back").css("transform", "translateX(-25vw)");
        $(".Punchbag .Filler").css("transform", "translateX(0px)");
        $(".Punchbag .Tarpaulin").css("transform", "translateX(15vw)");
        $(".Punchbag .Case-Cell").css("transform", "translateX(30vw)");
    }
});

$(window).load(function () {

    if (Resize_Width >= 1200) {
        if ($(window).scrollTop() > ($('.Inside-Background').offset().top - 400)) {

            $(".Punchbag .Top, .Punchbag .Back").css("transform", "translateX(-" + 80 * 2 + "px)");
            $(".Punchbag .Filler").css("transform", "translateX(0px)");
            $(".Punchbag .Tarpaulin").css("transform", "translateX(" + 70 * 2 + "px)");
            $(".Punchbag .Case-Cell").css("transform", "translateX(" + 90 * 3 + "px)");

        } else if ($(window).scrollTop() < ($('.Inside-Background').offset().top - 400)) {

            $(".Punchbag .Top, .Punchbag .Back").css("transform", "translateX(0px)");
            $(".Punchbag .Case-Cell").css("transform", "translateX(0px)");
            $(".Punchbag .Tarpaulin").css("transform", "translateX(0px)");
            $(".Punchbag .Filler").css("transform", "translateX(0px)");

        } else {
            return false;
        }

        Inside_Name();

    } else {

        $('.Inside-Name').addClass("Active");

        $(".Punchbag .Top, .Punchbag .Back").css("transform", "translateX(-25vw)");
        $(".Punchbag .Filler").css("transform", "translateX(0px)");
        $(".Punchbag .Tarpaulin").css("transform", "translateX(15vw)");
        $(".Punchbag .Case-Cell").css("transform", "translateX(30vw)");

    }
});


$(window).scroll(function () {
    if (Resize_Width >= 1200) {

        Inside_Name();

        if ($(window).scrollTop() > ($('.Inside-Background').offset().top - 400) && $(window).scrollTop() < $('.Inside-Background').offset().top) {

            scroll = $(window).scrollTop() - ($('.Inside-Background').offset().top - 400);

            $(".Punchbag .Top, .Punchbag .Back").css("transform", "translateX(-" + (scroll / 2) + "px)");
            $(".Punchbag .Tarpaulin").css("transform", "translateX(" + (scroll / 2.5) + "px)");
            $(".Punchbag .Case-Cell").css("transform", "translateX(" + (scroll / 1.3) + "px)");

        } else if ($(window).scrollTop() < ($('.Inside-Background').offset().top - 400)) {

            $(".Punchbag .Top, .Punchbag .Back").css("transform", "translateX(0px)");
            $(".Punchbag .Case-Cell").css("transform", "translateX(0px)");
            $(".Punchbag .Tarpaulin").css("transform", "translateX(0px)");
            $(".Punchbag .Filler").css("transform", "translateX(0px)");

        } else {
            return false;
        }
    } else {

        $(".Punchbag .Top, .Punchbag .Back").css("transform", "translateX(-25vw)");
        $(".Punchbag .Filler").css("transform", "translateX(0px)");
        $(".Punchbag .Tarpaulin").css("transform", "translateX(15vw)");
        $(".Punchbag .Case-Cell").css("transform", "translateX(30vw)");

    }
});


function Inside_Name() {
    if ($(window).scrollTop() > $('.Inside-Background').offset().top - 150) {
        $('.Inside-Name').addClass("Active");
    } else {
        $('.Inside-Name,.Tarpaulin, .Case-Cell, .Filler').removeClass("Active");
    }
}
