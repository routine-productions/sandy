<!doctype html>
<html>
<head></head>
<body>
    <div class="Header">
        <div class="Header-Logo"></div>
    </div>
</body>
</html>


1 Прототипирование
1.1 Карта сайта ( mindmeister )
1.2 Прототипы ( moquaps )
2 Разработка дизайна ( material design )
2.1 Структура сайта ( desktop )
2.2 Подбор графики
2.3 Иллюстрации
2.4 Адаптивные версии дизайна (mobile, tablet)
2.5 Нарезка графики
3 Верстка
3.1 Верстка html структуры
3.2 Создаем прототипы
3.3 Верстаем mobile -> tablet -> desktop
4 Программирование frontend
4.1 js
4.2 jQuery
4.3 Angular, Blackbone
5 Прогмамирование backend
5.1 php
5.2 mysql, neo4j
6 Наполнение сайта контентом
7 Размещение на хостинге